import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Image from "../components/image"

const IndexPage: React.FC = () => (
  <Layout image={<Image />}>
    <SEO title="Home" />
    <h1>Pardon our dust!</h1>
    <p>We're currently rebuilding our website.</p>
    <p>
      We're a high-speed, redundant, interoperable wide area network in the mid-south. We bring network connectivity to
      people and their infrastructure, independent of ISPs. Our network is designed for experimentation, education, and
      emergency use. By operating as licensed Amateur Radio stations, we are able to offer service with extreme range
      and impressive speed.
    </p>
    <p>
      MemHamWAN is live today with 7 points of presence and 6 cell sites. If you live in Shelby County or Tipton County,
      there's a good chance that you can connect! Reach out to our team over the chat for more.
    </p>
  </Layout>
)

export default IndexPage
