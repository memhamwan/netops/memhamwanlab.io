/** @jsxImportSource theme-ui */

type HeaderLinkProps = {
  href: string
  name: string
}

const HeaderLink: React.FC<HeaderLinkProps> = ({ href, name }) => (
  <a
    href={href}
    sx={{
      padding: `0.5rem`,
      margin: `0.5rem`,
      textDecoration: `none`,
      color: `rgba(255,255,255,0.8)`,
      "&:hover": {
        color: `rgba(255,255,255,1.0)`,
        transition: `0.3s`,
      },
      "&:visited": {
        color: `rgba(255,255,255,0.8)`,
      },
    }}
  >
    {name}
  </a>
)

export default HeaderLink
