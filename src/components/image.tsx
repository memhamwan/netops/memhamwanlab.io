import React from "react"
import { StaticImage } from "gatsby-plugin-image"

const Image: React.FC = () => {
  return <StaticImage src="../images/dishes.jpg" alt="" layout="fullWidth" />
}

export default Image
