/** @jsxImportSource theme-ui */
import { Link } from "gatsby"
import HeaderLink from "./header-link"
import type { Links } from "../types/links"

type HeaderProps = {
  siteTitle: string
  image: React.ReactNode
  links: Links
}

const Header: React.FC<HeaderProps> = ({ siteTitle, image, links }) => (
  <header
    sx={{
      background: `rgb(35,60,104)`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>
      {links.map(({ name, href }) => (
        <HeaderLink href={href} name={name} key={href} />
      ))}
    </div>
    {image}
  </header>
)

export default Header
